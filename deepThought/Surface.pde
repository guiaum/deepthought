// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example

// An uneven surface boundary

class Surface {
  // We'll keep track of all of the surface points
  ArrayList<Vec2> surface;
  float nbrPoints = 80;
  int radius = (height-50)/2;
  int x = width / 2;
  int y = height / 2;
  
  Body body;

  Surface() {
    surface = new ArrayList<Vec2>();

    // This is what box2d uses to put the surface in its world
    ChainShape chain = new ChainShape();


    // This has to go backwards so that the objects  bounce off the top of the surface
    // This "edgechain" will only work in one direction!
    //for (float x = width+10; x > -10; x -= 5) {


      /*
      // Doing some stuff with perlin noise to calculate a surface that points down on one side
       // and up on the other
       float y;
       if (x > width/2) {
       y = 100 + (width - x)*1.1 + map(noise(xoff),0,1,-80,80);
       } 
       else {
       y = 100 + x*1.1 + map(noise(xoff),0,1,-80,80);
       }
       */
      //void polygon(float x, float y, float radius, int npoints) {
      float angle = TWO_PI / nbrPoints;
      // beginShape();
      for (float a = 0; a < TWO_PI+angle; a += angle) {
        float sx = x + cos(a) * radius;
        float sy = y + sin(a) * radius;
        //vertex(sx, sy);
        //}
        //  endShape(CLOSE);


        // Store the vertex in screen coordinates
        surface.add(new Vec2(sx, sy));

      }

      



        // Build an array of vertices in Box2D coordinates
        // from the ArrayList we made
        Vec2[] vertices = new Vec2[surface.size()];
      for (int i = 0; i < vertices.length; i++) {
        Vec2 edge = box2d.coordPixelsToWorld(surface.get(i));
        vertices[i] = edge;
      }

      // Create the chain!
      chain.createChain(vertices, vertices.length);

      // The edge chain is now attached to a body via a fixture
      BodyDef bd = new BodyDef();
      bd.position.set(0.0f, 0.0f);
      body = box2d.createBody(bd);
      // Shortcut, we could define a fixture if we
      // want to specify frictions, restitution, etc.
      body.createFixture(chain, 1);
    }

    // A simple function to just draw the edge chain as a series of vertex points
    void display() {
      strokeWeight(2);
      stroke(200);
      noFill();
      beginShape();
      for (Vec2 v : surface) {
        vertex(v.x, v.y);
      }
      endShape();
    }
    
  void killBody()
  {
    box2d.destroyBody(body);
  }

}
