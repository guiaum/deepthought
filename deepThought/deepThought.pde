// AUTEUR: Guillaume Bertrand & Damien Gabriel 2018-2019
// http://gabrieldamien.free.fr
// licence GNU GPL v3
/*
//Différents modes
Mode 1 
Les particules se déplacent dans les 4 coins de l'écran

Mode 2
Les particules sont stables si on est dans l'objectif (focus central)

Mode 3
Les particules s'accumulent si on reste dans l'objectif (elles ont une durée de vie).

States
State 0 = wait
Possibilité de régler, plus tard possibilité de régler à distance et de déclencer le départ à distance
State 1 = playing
*/

//Box2d
import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import org.jbox2d.dynamics.contacts.*;

//Communication with Headset
import oscP5.*;
import netP5.*;
OscP5 oscP5;
NetAddress myRemoteLocation;
int OSCport = 57110;

boolean debug = false;

//EEG Values sent from headset
float valence =0;
float arousal=0;

float prevValence=0;
float prevArousal=0;

float smoothArousal=0;
float smoothValence=0;

int dividerArousal = 5;
int dividerValence = 5;

int objectifValence = 0;
int objectifArousal= 0;
int deltaValence = 0;
int deltaArousal = 0;


// BOX2D
// A reference to our box2d world
Box2DProcessing box2d;

// An ArrayList of particles that will fall on the surface
ArrayList<Mover> particles;

Attractor a;
// An object to store information about the uneven surface
Surface surface;

PImage bg;

long lastCreated;

float count = 25;
float actualCount = 0;

int lastFrame;
int previousFrame;

int state = 0;
int mode = 0;

//PARAMS
boolean attractorMode = false;
float correction;

PFont f;

void setup() 
{

  size(1024, 768, P3D);
  //fullScreen(P3D);
  smooth(6);
  oscP5 = new OscP5(this, OSCport);

  lastFrame = 0;
  previousFrame = 0;

  box2d = new Box2DProcessing(this);

  bg = loadImage("bg.png");
  
  f = loadFont("DejaVuSansMono-Bold-12.vlw");
  textFont(f);
}

void draw()
{
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  // VALEURS RECUES
  smoothValence = prevValence + (((valence - prevValence) / count) * actualCount);
  smoothArousal = prevArousal + (((arousal - prevArousal) / count) * actualCount);

  //On inverse le sens pour être plus cohérent (émotion positive = image reconstituée ou visible)
  if (mode == 1)
  {
    if (Float.isNaN(smoothValence) == false && Float.isNaN(smoothArousal) == false)
    {
      smoothValence = map (smoothValence, 0, 100, 100, 0);
      smoothArousal = map (smoothArousal, 0, 100, 100, 0);
    } else {
      smoothValence = 0;
      smoothArousal = 0;
    }
  }

  if (actualCount<count)
  {
    actualCount ++;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////


  switch (state)
  {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////  
  case 0:
    // WAITING
    background(0);
    fill(255);
    ellipseMode(CENTER);
    ellipse (width/2, height/2, height-50, height-50);
    textAlign(CENTER);
    int centerX = width/2;
    int centerY = height/2-150;
    int offset =  25;
    fill(80);
    text("DeepThought_Prototype", centerX, centerY);
    text("-", centerX, centerY+offset);
    text("Test 1 // diriger les particules.", centerX, centerY+offset*2);
    text("Test 2 // atteindre un objectif pour concentrer les particules.", centerX, centerY+offset*3);
    text("Test 3 // atteindre un objectif pour accumuler des particules.", centerX, centerY+offset*4);
    break;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
  case 1:
    background(0);
    fill(255);
    ellipseMode(CENTER);
    ellipse (width/2, height/2, height-50, height-50);
    textAlign(CENTER);
    fill(80);
    int centerX2 = width/2;
    int centerY2 = height/2-150;
    int offset2 =  25;
    text("DeepThought_Prototype", centerX2, centerY2);
    text("-", centerX2, centerY2+offset2);
    text("Test 1 // diriger les particules.", centerX2, centerY2+offset2*2);
    text("Test 2 // atteindre un objectif pour concentrer les particules.", centerX2, centerY2+offset2*3);
    text("Test 3 // atteindre un objectif pour accumuler des particules.", centerX2, centerY2+offset2*4);
    text("-", centerX2, centerY2+offset2*5);
    text("Objectifs 1, 3, 7 ou 9", centerX2, centerY2+offset2*6);
    text("-", centerX2, centerY2+offset2*7);
    text("1 // triste, déprimé ", centerX2, centerY2+offset2*8);
    text("3 // détendu et calme", centerX2, centerY2+offset2*9);
    text("7 // craintif et anxieux", centerX2, centerY2+offset2*10);
    text("9 // excité et joyeux", centerX2, centerY2+offset2*11);
    break;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
  case 10:
    println("10");
    box2d.createWorld();
    // Turn on collision listening!
    box2d.listenForCollisions();
    // No global gravity force
    box2d.setGravity(0, 0);


    // Create the empty list
    particles = new ArrayList<Mover>();
    //particles.add(new Mover(random(8, 16), random(width), random(height)));
    surface = new Surface();

    if (attractorMode || mode == 2) a = new Attractor(10, width/2, height/2);
    state = 20;
    break;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
  case 20:
    // Mode 1 
    // Les particules se baladent dans les 4 coins
    if (mode == 1)
    {  
      
      println("mode1");
      if (particles.size() < 800)
      {
        /*
      if (smoothValence>30)
         {
         */
        if (millis()-lastCreated>50)
        {
          particles.add(new Mover(random(4, 10), width/2, height/2));
          //if (smoothValence>70)particles.add(new Mover(random(4, int(smoothValence/5)), random(width), random(height)));
          lastCreated = millis();
        }
      }
      float dividedArousal = smoothArousal/dividerArousal;
      float dividedValence = smoothValence/dividerValence;
      box2d.setGravity(100/dividerValence/2 - dividedValence, 100/dividerArousal/2 - dividedArousal);
    } else if (mode == 2)
    {

      //////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Mode 2
      // Les particules sont attirées vers le centre si on est dans l'objectif.
      //////////////////////////////////////////////////////////////////////////////////////////////////////////
      
      if (particles.size() < 800)
      {
        /*
      if (smoothValence>30)
         {
         */
        if (millis()-lastCreated>50)
        {
          particles.add(new Mover(random(4, 10), width/2 + random(-100, 100), height/2+random(-100,100)));
          //if (smoothValence>70)particles.add(new Mover(random(4, int(smoothValence/5)), random(width), random(height)));
          lastCreated = millis();
        }
      }

      deltaValence = (int) abs(objectifValence - smoothValence);
      deltaArousal = (int) abs(objectifArousal - smoothArousal);
      float delta = (deltaArousal + deltaValence)/2;


      int maxDeltaValence = max (100 - objectifValence, objectifValence );
      int maxDeltaArousal = max (100 - objectifArousal, objectifArousal );
      int maxDelta = (maxDeltaValence+maxDeltaArousal)/2;

      correction = map(delta, 0, maxDelta, 0.9, -0.9);
      //println (smoothValence, smoothArousal, deltaValence,deltaArousal,delta, maxDeltaValence, maxDeltaArousal, maxDelta, correction );
    } else if (mode == 3)
    {

      //////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Mode 3
      // Plus on est dans l'objectif, plus les particules apparaissent.


      deltaValence = (int) abs(objectifValence - smoothValence);
      deltaArousal = (int) abs(objectifArousal - smoothArousal);
      float delta3 = (deltaArousal + deltaValence)/2;


      int maxDeltaValence3 = max (100 - objectifValence, objectifValence );
      int maxDeltaArousal3 = max (100 - objectifArousal, objectifArousal );
      int maxDelta3 = (maxDeltaValence3+maxDeltaArousal3)/2;

      correction = map(delta3, 0, maxDelta3, 5, 3000); 
      
      if (particles.size() < 1500)
      {
        /*
      if (smoothValence>30)
         {
         */
        if (millis()-lastCreated>correction)
        {
          particles.add(new Mover(random(4, 30), width /2, 60));
          //if (smoothValence>70)particles.add(new Mover(random(4, int(smoothValence/5)), random(width), random(height)));
          lastCreated = millis();
        }
      }

      box2d.setGravity(0, -10);
    }




    println("step");
    background(0);
    fill(255);
    ellipseMode(CENTER);
    ellipse (width/2, height/2, height-50, height-50);
    // We must always step through time!
    box2d.step();
    //surface.display();
    // Look at all particles
    for (int i = particles.size()-1; i >= 0; i--) {
      Mover p = particles.get(i);
      if (attractorMode || mode == 2 )
      {
        a.display();
        Vec2 force = a.attract(p);
        if (mode == 2)
        {

          force = force.mul(correction);
        }
        p.applyForce(force);
      }
      p.display();

      // Particles that leave the screen, we delete them
      // (note they have to be deleted from both the box2d world and our list
      if (p.done()) {
        particles.remove(i);
      }
    }
    
    if (mode == 3) surface.display();

    println("V:" +valence +" A:" +arousal+ " -- prevV:" +prevValence +" prevA:" +prevArousal+ " -- smoothV: " + smoothValence + " smoothA:" + smoothArousal);

    if (debug)
    {
      // DEBUG
      fill (255);
      noStroke();
      //1
      rect(0, 0, 10, valence);
      //2
      rect(20, 0, 10, arousal);
      fill (255, 0, 0);
      //3
      rect(40, 0, 10, prevValence);
      //4
      rect(60, 0, 10, prevArousal);
      fill (0, 255, 0);
      //5
      rect(80, 0, 10, smoothValence);
      //6
      rect(100, 0, 10, smoothArousal);
    }

    break;


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
  case 30:
    //END
    //background(0);
    for (int i = particles.size()-1; i >= 0; i--) {
      Mover p = particles.get(i);
      p.killBody();
    }
    particles.clear();
    if (attractorMode || mode == 2) a.killBody();
    surface.killBody();
    state = 0;

  }
}



void keyReleased() {
  switch (state)
  {
  case 0:
    // WAITING
    if (key == '1') 
    {
      mode = 1;
      state = 10;
    } else if (key == '2') 
    {
      mode = 2; 
      state = 1;
    } else if (key == '3') 
    {
      mode = 3; 
      state = 1;
    }
    break;

  case 1:
    // IF NEEDED, CHOICE OF PARAMS
    // WAITING
    if (key == '1') 
    {
      objectifValence = 25;
      objectifArousal= 25;
      state = 10;
    } else if (key == '3') 
    {
      objectifValence = 75;
      objectifArousal= 25;
      state = 10;
    } else if (key == '7') 
    {
      objectifValence = 25;
      objectifArousal= 75; 
      state = 10;
    } else if (key == '9') 
    {
      objectifValence = 75;
      objectifArousal= 75; 
      state = 10;
    }
    break;
    
  case 20:
  if (key ==' ') state = 30;
  break;
  }
}

void oscEvent(OscMessage theOscMessage) {

  lastFrame = frameCount;
  count = lastFrame - previousFrame;
  previousFrame = lastFrame;
  //if (debug)println ("Count: "+ count);
  actualCount = 0; 

  if (theOscMessage.checkTypetag("ii")) {

    prevValence = valence;
    prevArousal = arousal;

    valence = theOscMessage.get(0).intValue();  // get the first osc argument
    arousal = theOscMessage.get(1).intValue();  // get the first osc argument
  }
}

// Collision event functions!
void beginContact(Contact cp) {
  // Get both shapes
  Fixture f1 = cp.getFixtureA();
  Fixture f2 = cp.getFixtureB();
  // Get both bodies
  Body b1 = f1.getBody();
  Body b2 = f2.getBody();

  // Get our objects that reference these bodies
  Object o1 = b1.getUserData();
  Object o2 = b2.getUserData();

  if (o1.getClass() == Attractor.class) {
    Mover p = (Mover) o2;
    p.delete();
  }
  if (o2.getClass() == Attractor.class) {
    Mover p = (Mover) o1;
    p.delete();
  }
}

// Objects stop touching each other
void endContact(Contact cp) {
}
