# DeepThought

Système de visualisation des émotions, porté par Damien Gabriel, Membre du Laboratoire de Neurosciences de Besançon

En 2018, dans le cadre de l’Appel à projets artistiques de l’Université de Franche-Comté et de la DRAC, le
chercheur Damien Gabriel s’est associé à Guillaume Bertrand, artiste numérique. 

La recherche est le prolongement d'expériences menées par le laboratoire pour aider à la visualisation en temps réél des émotions du patient, lues à l'aide d'un casque EEG.
Le but est d'offrir un Neurofeedback non punitif, fluide, et ainsi favoriser la maîtrise de l'état émotionnel du patient, pour aider les patients dépressifs, mais aussi des jeunes
en crise d’adolescence, des sportifs ou même, au travail, à manager au mieux des équipes.

En avril dernier, le dispositif conçu par Damien Gabriel et Guillaume Bertrand a remporté un accessit du
prix  AEMD  Marina  Picasso. 

Programme réalisé grâce au logiciel Processing, il nécessite les bibliothèques OSCP5 d'Andreas Schlegel et Box2D de Daniel Shiffman.

Le programme attends des valeurs de valence et d'arousal envoyées en OSC. Le programme dummySender permet d'envoyer des valeurs aléatoires.
Un programme développé par le laboratoire permet de réaliser l'interface entre le casque et deepThought.

Programme distribué sous licence GNU GPL v3.
