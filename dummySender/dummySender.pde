/**
 * oscP5sendreceive by andreas schlegel
 * example shows how to send and receive osc messages.
 * oscP5 website at http://www.sojamo.de/oscP5
 */
 
import oscP5.*;
import netP5.*;
  
OscP5 oscP5;
NetAddress myRemoteLocation;

long lastSent = 0;
int interval = 1000;

void setup() {
  size(100,100);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,9001);
  
  /* myRemoteLocation is a NetAddress. a NetAddress takes 2 parameters,
   * an ip address and a port number. myRemoteLocation is used as parameter in
   * oscP5.send() when sending osc packets to another computer, device, 
   * application. usage see below. for testing purposes the listening port
   * and the port of the remote location address are the same, hence you will
   * send messages back to this sketch.
   */
  myRemoteLocation = new NetAddress("127.0.0.1",57110);
}


void draw() {
  background(0);  
  if (millis()-lastSent > interval)
  {
      /* in the following different ways of creating osc messages are shown by example */
      int value1 = (int) random(100);
      int value2 = (int) random(100);
      OscMessage myMessage = new OscMessage("/neuro");
      myMessage.add(value1); /* add an int to the osc message */
      myMessage.add(value2); /* add an int to the osc message */
      /* send the message */
      oscP5.send(myMessage, myRemoteLocation); 
      lastSent = millis();
      println(value1, value2);
    
  }
}
